package me.jeroen.ftag;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.jeroen.coreplugin.api.API;
import me.jeroen.ftag.commands.PlayerSenderMsg;
import me.jeroen.ftag.enums.GameState;
import me.jeroen.ftag.events.BlockListener;
import me.jeroen.ftag.events.DamageListener;
import me.jeroen.ftag.events.KitListener;
import me.jeroen.ftag.timers.FreezeTimer;
import me.jeroen.ftag.timers.GameTimer;
import me.jeroen.ftag.timers.KitTimer;
import me.jeroen.ftag.timers.PreGameTimer;
import me.jeroen.ftag.timers.ResetTimer;
import me.jeroen.ftag.utilities.FileHand;
import me.jeroen.ftag.utilities.MoreKit;
import me.jeroen.ftag.utilities.SMsg;
import me.jeroen.ftag.utilities.TeleportP;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.kitteh.tag.TagAPI;

public class Core extends JavaPlugin {

	public static Logger log;
	private final static Logger logger = Logger.getLogger("Minecraft");
	public static Core instance;
	public static ReentrantLock lock = new ReentrantLock(true);
	public static GameState GAMESTATE = GameState.PREGAME;
	
	public static Location LobbySpawn = null;
	public static ArrayList<String> PlayerSpawn = new ArrayList<String>();
	public static Location TaggerSpawn = null;
	
	public static Integer framt =0;
	
	public static String MapName = null;
	
	public static String MapBefore = null;

	public static Integer COUNTDOWN = 0;
	public static Integer COUNTDOWN_SECONDS = 0;
	public static Integer COUNTDOWN_GAME = 0;

	public static ArrayList<String> FrozenPlayers = new ArrayList<String>();
	public static ArrayList<String> Freezers = new ArrayList<String>();
	public static HashMap<String, Location> FrozenLoc = new HashMap<String,Location>();
	public static HashMap<UUID, String> Kits = new HashMap<UUID, String>();
	
	public static HashMap<UUID, Long> CanGetDisks = new HashMap<UUID,Long>();
	public static HashMap<UUID, Integer> KickDiskF =  new HashMap<UUID, Integer>();

	//public static Player IsFreezer = null;
	public static Integer COUNTDOWN_INGAME = 0;
	
	public Random r = new Random();

	public void onEnable() {
		
		PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new DamageListener(this), this);
        pm.registerEvents(new BlockListener(this), this);
        pm.registerEvents(new KitListener(this), this);

		instance = this;
		log = getPluginLogger();

		setupTheConfig();
		saveConfig();
		
        try{
			new FileHand();
		}catch(Exception e){
			
		}
        
        MapBefore = getConfig().getString("NOEDIT.MapBefore");
		
        new me.jeroen.ftag.utilities.Kits();
        new MoreKit();
		
		getCommand("fr").setExecutor(new PlayerSenderMsg(this));
		getCommand("kill").setExecutor(new PlayerSenderMsg(this));
		
		COUNTDOWN = getConfig().getInt("Config.pregametime");
		COUNTDOWN_SECONDS = getConfig().getInt("Config.pregametime");
		COUNTDOWN_GAME = getConfig().getInt("Config.ingametime");
		COUNTDOWN_INGAME  = getConfig().getInt("Config.ingametime")+20;
		
		if(getConfig().get("NOEDIT.Lobby")!=null){
			LobbySpawn = new Location(Bukkit.getWorld(getConfig().getString("NOEDIT.Lobby.world")), getConfig().getDouble("NOEDIT.Lobby.x"), getConfig().getDouble("NOEDIT.Lobby.y"), getConfig().getDouble("NOEDIT.Lobby.z"));
		}


		ChooseMap();
		new PreGameTimer();
		new KitTimer();
	}

	public void onDisable() {
		GameTimer.cancel();
		KitTimer.cancel();
		PreGameTimer.cancel();
		ResetTimer.cancel();
		FreezeTimer.cancel();
		log("&4Has been disabled!");
	}

	private void ChooseMap(){
		int mapNrr = r.nextInt(getConfig().getInt("NOEDIT.MapAmt")+1);
		int mapNr = mapNrr;
		log(""+mapNr);
		
		
		if(getConfig().get("Maps.Map"+mapNr)!=null && !MapBefore.equals(getConfig().getString("Maps.Map"+mapNr+".name"))){
			
			String wname = getConfig().getString("Maps.Map"+mapNr+".tgspawn.world");
			World w = Bukkit.getServer().getWorld(wname);
			log(wname);
			TaggerSpawn = new Location(w, getConfig().getInt("Maps.Map"+mapNr+".tgspawn.x"),getConfig().getInt("Maps.Map"+mapNr+".tgspawn.y"),getConfig().getInt("Maps.Map"+mapNr+".tgspawn.z"));
			
			int spamt = getConfig().getInt("Maps.Map"+mapNr+".SpawnAmt");
			for (int i = 1; i < spamt+1; i++){
				if(getConfig().get("Maps.Map"+mapNr+".plspawn"+i+".world") != null){
					String world = getConfig().getString("Maps.Map"+mapNr+".plspawn"+i+".world");
					int x = getConfig().getInt("Maps.Map"+mapNr+".plspawn"+i+".x");
					int y = getConfig().getInt("Maps.Map"+mapNr+".plspawn"+i+".y");
					int z = getConfig().getInt("Maps.Map"+mapNr+".plspawn"+i+".z");
					String fstr = world+","+x+","+y+","+z;
					System.out.println("Num:"+i+"; Spawn:"+fstr);
					PlayerSpawn.add(fstr);
				}				
			}
		
			
			
			log("&6Map will be: &2"+getConfig().getString("Maps.Map"+mapNr+".name"));
			MapName = getConfig().getString("Maps.Map"+mapNr+".name");
			
			getConfig().set("NOEDIT.MapBefore", getConfig().getString("Maps.Map"+mapNr+".name"));
			saveConfig();
		}else{
			ChooseMap();
		}
		
		
	}


	private void setupTheConfig() {
		if (getConfig().get("Config") == null) {
			getConfig().set("Config.pregametime", 120);
			getConfig().set("Config.ingametime", 300);
			getConfig().set("Config.giveCoinsOnTWin", 40);
			getConfig().set("Config.giveCoinsOnHWin", 10);
			getConfig().set("Config.giveCoinsTFreeze", 2);
			getConfig().set("Config.giveCoinsHUnFreeze", 1);
			saveConfig();
		}
		
		if (getConfig().get("NOEDIT") == null){
			getConfig().set("NOEDIT.MapAmt", 0);
			saveConfig();
		}
		
		if(getConfig().get("NOEDIT.MapBefore")==null){
			getConfig().set("NOEDIT.MapBefore", "none");
			saveConfig();
		}

	}

	public static void log(String message, Level level) {
		if (!message.isEmpty())
			logger.log(level, ("[FreezeTag] " + message));
	}

	public void log(String message) {
		getServer().getConsoleSender().sendMessage(
				ChatColor.translateAlternateColorCodes('&',
						"&7[&bFreezeTag&7] &2" + message));
	}

	public static Logger getPluginLogger() {
		return instance.getLogger();
	}

	@SuppressWarnings("deprecation")
	public static void startgame() {
		PreGameTimer.cancel();
		new FreezeTimer();
		
		if (Bukkit.getOnlinePlayers().length >= 6){
			framt = 2;
		}else{
			framt = 1;
		}

		while (Freezers.size() != framt) {
			ChooseIt();
		}
		
		for (Player p: Bukkit.getOnlinePlayers()){
			
			Bukkit.getServer().createWorld(new WorldCreator(MapName));
			if(!Freezers.contains(p.getName())){
				p.teleport(TeleportP.tpPlayer(p));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 9999999, 1));
			}else{
				p.teleport(new Location(Bukkit.getWorld(MapName),TaggerSpawn.getX(),TaggerSpawn.getY(),TaggerSpawn.getZ()));
				p.getInventory().setHelmet(new ItemStack(397,1,(byte)1));
				p.updateInventory();
				TagAPI.refreshPlayer(p);
			}
			p.playSound(p.getLocation(), Sound.ANVIL_LAND, 1, 1);	
		}
		
		MoreKit.gkitt();
		
		for (Player p:Bukkit.getOnlinePlayers()){
			if(Freezers.contains(p.getName())){
				SMsg.sendMsg("&6Choosed &b&l" + p.getName() + " &6As it!");		
			}
		}
		
		SMsg.sendMsg("&4Runners start running!");
		
		new GameTimer();

	}

	@SuppressWarnings("deprecation")
	public static void ChooseIt() {

		int random = new Random().nextInt(Bukkit.getOnlinePlayers().length);
		Player p = Bukkit.getOnlinePlayers()[random];

		if (p.isOnline() && !Freezers.contains(p.getName())){
			Freezers.add(p.getName());
		}

	}
	
	public static boolean hasKit(UUID PlayerID, String kitName){
		return API.hasFreezetagKit(PlayerID, kitName);
	}
	
	public static void copy(InputStream in, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
