package me.jeroen.ftag.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import me.jeroen.ftag.Core;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class MoreKit {
	
	static HashMap<Player, IconMenu> MENUS = new HashMap<Player, IconMenu>();

	public static void printKitChat(Player player) {

		Set<String> kits = FileHand.kitconf.getKeys(false);
		
		Integer invsize = 9;
		for (int i = 0; i <= 10; i++) {
			if ((i * 9) >= kits.size()) {
				invsize = invsize + i * 9;
				break;
			}
		}


		if (MENUS.containsKey(player)) {
			MENUS.get(player).destroy();
			MENUS.remove(player);
		}

		final Player pl = player;
		IconMenu menu = new IconMenu("Select a kit", pl.getName(), invsize,
				new IconMenu.OptionClickEventHandler() {
					public void onOptionClick(IconMenu.OptionClickEvent event) {
						Kits.setKit(pl, ChatColor.stripColor(event.getName()));
						event.setWillClose(true);
						event.setWillDestroy(false);
					}
				}, Core.instance);

		Integer mypos = 0;
		Integer othpos = 1;
		for (String kitname : kits) {
			try {

				char[] stringArray = kitname.toCharArray();
				kitname = new String(stringArray);

				ArrayList<String> container = new ArrayList<String>();
				ConfigurationSection kit = FileHand.kitconf.getConfigurationSection(kitname);
				
				List<String> kititems = kit.getStringList("DESCRIPTION");
				for (String item : kititems) {
					container.add(ChatColor.AQUA+item);
				}

				Integer itemid = kit.getInt("ITEMMENU");
				@SuppressWarnings("deprecation")
				Material kitem = Material.getMaterial(itemid);

				if (Core.hasKit(player.getUniqueId(), kitname)){
					
					container.add(ChatColor.GOLD+"-----------------------");
					container.add(ChatColor.DARK_GRAY+"Status: "+ChatColor.GREEN+"UNLOCKED");
					
					String[] info = new String[container.size()];
					info = container.toArray(info);

					menu.setOption(invsize - othpos, new ItemStack(kitem, 1),
							ChatColor.GRAY+""+ChatColor.BOLD + kitname, info);
					othpos++;
				} else {
					container.add(ChatColor.GOLD+"-----------------------");
					container.add(ChatColor.DARK_GRAY+"Status: "+ChatColor.RED+"LOCKED");

					String[] info = new String[container.size()];
					info = container.toArray(info);

					menu.setOption(mypos, new ItemStack(kitem, 1),
							ChatColor.GRAY+""+ChatColor.BOLD + kitname, info);
					mypos++;
				}
				container.clear();
			} catch (Exception e) {
				Core.log("Error while trying to parse kit '" + kitname + "'",
						Level.WARNING);
				e.printStackTrace();
			}
		}
		MENUS.put(pl, menu);
		menu.open(player);
	}
	
	@SuppressWarnings("deprecation")
	public static void gkitt(){
		for (Player p : Bukkit.getOnlinePlayers()){
			Kits.giveKit(p);	
		}
	}

}
