package me.jeroen.ftag.utilities;

import java.io.File;
import java.util.logging.Level;

import me.jeroen.ftag.Core;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileHand {
	
public static FileConfiguration kitconf;
	
	public FileHand() {		
		try{
			loadFiles();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void loadFiles() throws Exception {
		File kitFile = new File(Core.instance.getDataFolder(), "kit.yml");
		Integer creation = 0;

		if (!kitFile.exists()) {
			kitFile.getParentFile().mkdirs();
			Core.copy(Core.instance.getResource("kit.yml"), kitFile);
			creation++;
		}

		if(creation > 0)
			Core.log("Created " + creation + " files.",Level.INFO);

		kitconf = YamlConfiguration.loadConfiguration(
				new File(Core.instance.getDataFolder(), "kit.yml"));
	}

}
