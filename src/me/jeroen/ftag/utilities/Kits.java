package me.jeroen.ftag.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import me.jeroen.ftag.Core;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Kits {
	
	public static ArrayList<String> kits = new ArrayList<String>();
	public static ItemStack blazeRod = setNameAn(new ItemStack(Material.BLAZE_ROD), ChatColor.BLUE + "" + ChatColor.BOLD + "Click to get Speed!", null);
	public static ItemStack starneth = setNameAn(new ItemStack(Material.NETHER_STAR), ChatColor.BLUE + "" + ChatColor.BOLD + "Click to unfreeze everyone!", null);
	public static ItemStack quartnet = setNameAn(new ItemStack(Material.QUARTZ), ChatColor.BLUE + "" + ChatColor.BOLD + "Click to be invisible!", null);

	
	
	public Kits() {
		Set<String> kitList = FileHand.kitconf.getKeys(false);
		for (String kit : kitList) {

			kits.add(kit);
		}
	}

	public static void setKit(Player player, String kitname) {
		
		
		kitname = kitname.replace(".", "");
		ConfigurationSection kit = FileHand.kitconf
				.getConfigurationSection(kitname);

		if (kit == null && !kits.contains(kitname)) {
			SMsg.sendMsg(player, ChatColor.translateAlternateColorCodes('&',
					"&cThis kit does not exist :("));
		}

		if (Core.hasKit(player.getUniqueId(), kitname)) {
			
			if(Core.Kits.containsKey(player.getUniqueId())){
				Core.Kits.remove(player.getUniqueId());
			}
			
			Core.Kits.put(player.getUniqueId(), kitname);
			SMsg.sendMsg(player, "&2You have selected: &e"+kitname+" &2As your kit!");
		
		}else{
			SMsg.sendMsg(player, "&c&lYou do not have unlocked this kit!");
		}
		
	}

	public static int getCoins(String kitName) {
		ConfigurationSection def = FileHand.kitconf
				.getConfigurationSection(kitName);
		return def.getInt("COINS");
	}

	public static boolean isKit(String kitName) {
		return kits.contains(kitName);
	}
	
	
	
	
	
	
	
	
	@SuppressWarnings("deprecation")
	public static void giveKit(Player p) {
		p.getInventory().clear();
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		p.setExp(0);
		p.setLevel(0);

		if (Core.Kits.containsKey(p.getUniqueId())) {
			if(Core.Kits.get(p.getUniqueId()).equals("speeder")){
				p.getInventory().addItem(blazeRod);
				p.updateInventory();
			}
			if(Core.Kits.get(p.getUniqueId()).equals("thawer")){
				p.getInventory().addItem(starneth);
				p.updateInventory();
			}
			if(Core.Kits.get(p.getUniqueId()).equals("spyman")){
				p.getInventory().addItem(quartnet);
				p.updateInventory();
			}
			
		}
	}
	
	private static ItemStack setNameAn(ItemStack is, String name,
			List<String> lore) {
		ItemMeta im = is.getItemMeta();
		if (name != null) {
			im.setDisplayName(ChatColor.DARK_PURPLE + name);
		}
		if (lore != null) {
			im.setLore(lore);
		}
		is.setItemMeta(im);

		return is;
	}

}
