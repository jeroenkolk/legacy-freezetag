package me.jeroen.ftag.utilities;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import me.jeroen.coreplugin.api.API;
import me.jeroen.ftag.Core;
import me.jeroen.ftag.enums.GameState;
import me.jeroen.ftag.timers.GameTimer;
import me.jeroen.ftag.timers.ResetTimer;

public class CheckWinner {
	
	public static void checkForIt(){
		if(Core.Freezers.size() == 0){
			GameTimer.cancel();
			SMsg.sendMsg("&bIt left the game! &6&lServer restarting!");
			Core.GAMESTATE = GameState.RESETTING;
			Core.COUNTDOWN = 20;
			new ResetTimer();	
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void checkFreezerFreeze(){
		if(Core.FrozenPlayers.size() == Bukkit.getOnlinePlayers().length-Core.framt){
			GameTimer.cancel();
			SMsg.sendMsg("&b&lThe freezer have frozen everyone!");
			SMsg.sendMsg("&6&lServer restarting in 30 seconds!");
			for (Player p : Bukkit.getOnlinePlayers()){
				p.playSound(p.getLocation(), Sound.ENDERDRAGON_DEATH, 1, 1);
				p.getInventory().clear();
				p.updateInventory();
				if(Core.Freezers.contains(p.getName())){
					API.giveDisks(p.getName(), Core.instance.getConfig().getInt("Config.giveCoinsOnTWin"));
				}
			}
			Core.GAMESTATE = GameState.RESETTING;
			Core.COUNTDOWN = 20;
			new ResetTimer();	
		}
	}

}
