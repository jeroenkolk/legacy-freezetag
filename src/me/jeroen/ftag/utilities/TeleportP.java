package me.jeroen.ftag.utilities;

import java.util.Random;

import me.jeroen.ftag.Core;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TeleportP {
	
	private static Random r = new Random();
	
	public static Location tpPlayer(Player p) {
		int nrp = r.nextInt((Core.PlayerSpawn.size()));
		if(Core.PlayerSpawn.get(nrp) != null){
			String[] s = Core.PlayerSpawn.get(nrp).split(",");
			
			String w = s[0];
			int x = Integer.parseInt(s[1]);
			int y = Integer.parseInt(s[2]);
			int z = Integer.parseInt(s[3]);
			
			Location l = new Location(Bukkit.getWorld(w),x,y,z).add(0.5, 0, 0.5);
			return l;	
		}else{
			return tpPlayer(p);
		}
	}

}
