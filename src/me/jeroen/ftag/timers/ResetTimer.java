package me.jeroen.ftag.timers;

import me.jeroen.ftag.Core;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ResetTimer {
	
	private static Integer shed_id = null;

	public ResetTimer() {
		
		FreezeTimer.cancel();
		
		shed_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Core.instance, new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
				if (Core.COUNTDOWN.intValue() > 0) {
					
					if(Core.COUNTDOWN.intValue() == 5){
						for(Player p : Bukkit.getOnlinePlayers()){
							p.kickPlayer(ChatColor.GOLD+"Server is restarting!");
						}
					}

					Core.COUNTDOWN--;
				}else{
					Bukkit.getServer().shutdown();
				}
			}
		}, 0, 20);
	}

	public static void cancel() {
		if(shed_id != null) {
			Bukkit.getServer().getScheduler().cancelTask(shed_id);
			shed_id = null;
		}
	}

}
