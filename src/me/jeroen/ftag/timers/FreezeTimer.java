package me.jeroen.ftag.timers;

import me.jeroen.ftag.Core;
import me.jeroen.ftag.enums.ParticleEffect;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class FreezeTimer {
	
	private static Integer shed_id = null;

	public FreezeTimer() {
		
		
		shed_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Core.instance, new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
				
				for(String s : Core.FrozenPlayers){
					if(Bukkit.getPlayer(s) != null){
						Player p = Bukkit.getPlayer(s);
						ParticleEffect.CLOUD.play(p.getLocation(), 0, (float) 0.5, 0, (float) 0.03, 20);
					}
				}
				
				for(String s : Core.Freezers){
					if(Bukkit.getPlayer(s) != null){
						Player p = Bukkit.getPlayer(s);
						ParticleEffect.FLAME.play(p.getLocation(), 0, 0, 0, (float) 0.03, 10);
					}
				}
				
			}
		}, 0, 20/10);
	}

	public static void cancel() {
		if(shed_id != null) {
			Bukkit.getServer().getScheduler().cancelTask(shed_id);
			shed_id = null;
		}
	}

}
