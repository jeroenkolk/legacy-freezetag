package me.jeroen.ftag.timers;

import me.jeroen.ftag.Core;
import me.jeroen.ftag.enums.GameState;
import me.jeroen.ftag.utilities.SMsg;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class PreGameTimer {
	
	private static Integer shed_id = null;

	public PreGameTimer() {
		
		shed_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Core.instance, new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
				if (Core.COUNTDOWN.intValue() > 0) {
				
					
					
					
					if (Core.COUNTDOWN >= 11 & Core.COUNTDOWN % 10 == 0) {
						SMsg.sendMsg("&eGame starting in: &6&l<TIME> &eSeconds".replaceAll("<TIME>", ""+Core.COUNTDOWN));
						for(Player pl :Bukkit.getOnlinePlayers()){
							pl.sendMessage("--------------------------------");
						}
						SMsg.sendMsg("&8Map choosen: &7&l"+Core.MapName);
					} else if (Core.COUNTDOWN < 11) {
						SMsg.sendMsg("&cGame starting in: &4&l<TIME> &cSeconds".replaceAll("<TIME>", ""+Core.COUNTDOWN));
						for (Player pl : Bukkit.getOnlinePlayers()) {
							pl.playSound(pl.getLocation(), Sound.CLICK, 1.0F, (byte) 1);
						}
					}

					Core.COUNTDOWN--;
				}else if (Bukkit.getOnlinePlayers().length >= 2) {
					Core.GAMESTATE = GameState.INGAME;
					Core.startgame();
				} else {
					SMsg.sendMsg("&6Not enouch player to start the game. Restarting Countdown!");
					Core.COUNTDOWN = Core.COUNTDOWN_SECONDS;
				}
			}
		}, 0, 20);
	}

	public static void cancel() {
		if(shed_id != null) {
			Bukkit.getServer().getScheduler().cancelTask(shed_id);
			shed_id = null;
		}
	}

}
