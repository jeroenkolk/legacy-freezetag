package me.jeroen.ftag.timers;

import java.util.HashMap;
import java.util.UUID;

import me.jeroen.ftag.Core;
import me.jeroen.ftag.utilities.SMsg;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class KitTimer {
	
	private static Integer shed_id = null;

	public static HashMap<UUID, Integer> KitTimeout = new HashMap<UUID, Integer>();

	public KitTimer() {
		shed_id = Bukkit.getServer().getScheduler()
				.scheduleSyncRepeatingTask(Core.instance, new Runnable() {

					@SuppressWarnings("deprecation")
					public void run() {

						if (!KitTimeout.isEmpty()) {
							for(UUID key : KitTimeout.keySet()){
								if(KitTimeout.get(key)!=null){
									Integer timel = KitTimeout.get(key);
									if(timel == 0){
										KitTimeout.remove(key);
										return;
									}else{
										if(Core.Kits.get(key).equals("speeder")){
											if(timel == 499){
												for (PotionEffect effect : Bukkit.getPlayer(key).getActivePotionEffects())
													Bukkit.getPlayer(key).removePotionEffect(effect.getType());
											}else if(timel == 497){
												if(!Core.Freezers.contains(Bukkit.getPlayer(key).getName())){
													Bukkit.getPlayer(key).addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 10000000,1));	
												}
											}else if(timel == 599){
												Bukkit.getPlayer(key).addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000,4));
											}
										}else if(Core.Kits.get(key).equals("spyman")){
											if(timel == 500){
												for(Player p:Bukkit.getOnlinePlayers()){
													p.showPlayer(Bukkit.getPlayer(key));
												}
												
												SMsg.sendMsg(Bukkit.getPlayer(key), "&bYou are visible again!");
											}
										}
										
										
										int newval = timel-1;
										KitTimeout.put(key, newval);
									}
								}
							}
						}

					}

				}, 0, 20/10);
	}

	public static void cancel() {
		if (shed_id != null) {
			Bukkit.getServer().getScheduler().cancelTask(shed_id);
			shed_id = null;
		}
	}


}
