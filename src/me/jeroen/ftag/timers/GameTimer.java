package me.jeroen.ftag.timers;

import me.jeroen.coreplugin.api.API;
import me.jeroen.ftag.Core;
import me.jeroen.ftag.enums.GameState;
import me.jeroen.ftag.utilities.CheckWinner;
import me.jeroen.ftag.utilities.SMsg;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class GameTimer {
	
	private static Integer shed_id = null;

	public GameTimer() {
		
		
		shed_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Core.instance, new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
				
				
				if (Core.COUNTDOWN_INGAME.intValue() > 0) {
				
					
					if (Core.COUNTDOWN_INGAME <= Core.COUNTDOWN_GAME){
						CheckWinner.checkForIt();
						CheckWinner.checkFreezerFreeze();
					}
					
					

					Core.COUNTDOWN_INGAME--;
				}else{
					SMsg.sendMsg("&e&lThats a round!");
					SMsg.sendMsg("&5Server resetting in 30 seconds!");
					for (Player p : Bukkit.getOnlinePlayers()){
						p.getInventory().clear();
						p.updateInventory();
						if(!Core.Freezers.contains(p.getName())){
							API.giveDisks(p.getName(), Core.instance.getConfig().getInt("Config.giveCoinsOnHWin"));
						}
					}
					
					Core.GAMESTATE = GameState.RESETTING;
					Core.COUNTDOWN = 20;
					new ResetTimer();
					cancel();
				}
			}
		}, 0, 20);
	}

	public static void cancel() {
		if(shed_id != null) {
			Bukkit.getServer().getScheduler().cancelTask(shed_id);
			shed_id = null;
		}
	}

}
