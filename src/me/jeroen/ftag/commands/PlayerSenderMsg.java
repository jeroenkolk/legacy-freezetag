package me.jeroen.ftag.commands;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.jeroen.ftag.Core;
import me.jeroen.ftag.utilities.SMsg;

public class PlayerSenderMsg implements CommandExecutor{
	
	public Core plugin;
	public PlayerSenderMsg(Core ins){
		plugin = ins;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String CommandLabel, String[] args){
		if (!CommandLabel.equalsIgnoreCase("fr")) return true;
		
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(p.isOp()){
				if(args.length == 0){
					sendHelpMsg(p);
				}else if(args.length == 1 && args[0].equals("setlobby")){
					plugin.getConfig().set("NOEDIT.Lobby.world", p.getWorld().getName());;
					plugin.getConfig().set("NOEDIT.Lobby.x", p.getLocation().getBlockX());
					plugin.getConfig().set("NOEDIT.Lobby.y", p.getLocation().getBlockY());
					plugin.getConfig().set("NOEDIT.Lobby.z", p.getLocation().getBlockZ());
					plugin.saveConfig();
					SMsg.sendMsg(p, "&2Succzesfull created/moved the lobbyspawn!");
				}else if(args.length == 2 && args[0].equals("addmap")){
					int oldnr = plugin.getConfig().getInt("NOEDIT.MapAmt");
					int newnr = oldnr+1;
					plugin.getConfig().set("Maps.Map"+newnr+".name", args[1]);
					plugin.getConfig().set("NOEDIT.MapAmt", newnr);
					plugin.saveConfig();
					SMsg.sendMsg(p, "&2Succzesfull created map#1 with name #2".replaceAll("#1", ""+newnr).replaceAll("#2", args[1]));
				}else if(args.length == 2 && args[0].equals("tspawn")){
					if(plugin.getConfig().get("Maps.Map"+args[1])!=null){
						
						plugin.getConfig().set("Maps.Map"+args[1]+".tgspawn.world", p.getWorld().getName());;
						plugin.getConfig().set("Maps.Map"+args[1]+".tgspawn.x", p.getLocation().getBlockX());
						plugin.getConfig().set("Maps.Map"+args[1]+".tgspawn.y", p.getLocation().getBlockY());
						plugin.getConfig().set("Maps.Map"+args[1]+".tgspawn.z", p.getLocation().getBlockZ());
						plugin.saveConfig();
						
						SMsg.sendMsg(p,"&2Succzesfull added the tagger spawn to the map!");
						
					}else{
						SMsg.sendMsg(p, "&4This map nr does not exist!");
					}
				}else if(args.length == 2 && args[0].equals("pspawn")){
					
					if(plugin.getConfig().get("Maps.Map"+args[1])!=null){
						
						if(plugin.getConfig().get("Maps.Map"+args[1]+".SpawnAmt") == null){
							plugin.getConfig().set("Maps.Map"+args[1]+".SpawnAmt", 0);
						}
						
						int amt = plugin.getConfig().getInt("Maps.Map"+args[1]+".SpawnAmt")+1;
						
						plugin.getConfig().set("Maps.Map"+args[1]+".plspawn"+amt+".world", p.getWorld().getName());;
						plugin.getConfig().set("Maps.Map"+args[1]+".plspawn"+amt+".x", p.getLocation().getBlockX());
						plugin.getConfig().set("Maps.Map"+args[1]+".plspawn"+amt+".y", p.getLocation().getBlockY());
						plugin.getConfig().set("Maps.Map"+args[1]+".plspawn"+amt+".z", p.getLocation().getBlockZ());
						plugin.saveConfig();
						
						plugin.getConfig().set("Maps.Map"+args[1]+".SpawnAmt", amt);
						
						SMsg.sendMsg(p,"&2Succzesfull added the player spawn to the map!");
						
						
					}else{
						SMsg.sendMsg(p, "&4This map nr does not exist!");
					}
					
					
				}else if(args.length == 2 && args[0].equals("tp")){
					
					Bukkit.getServer().createWorld(new WorldCreator(args[1]));
					p.teleport(new Location(Bukkit.getWorld(args[1]), 0,64,0));
					
					
				}else{
					sendHelpMsg(p);
				}
			}else{
				SMsg.sendMsg(p, "&4You are not permitted to peform this command!");
			}
		}else{
			
			Core.log("Only players can do this command! "+Core.MapName, Level.WARNING);
		}
		return false;
	}
	
	private void sendHelpMsg(Player p){
		SMsg.sendMsg(p, "&e/fr &6{Prints the help of freezetag}");
		SMsg.sendMsg(p, "&e/fr addmap [MapName] &6{Creates a map}");
		SMsg.sendMsg(p, "&e/fr tp [World] &6{Tps you to the world}");
		SMsg.sendMsg(p, "&e/fr tspawn [MapNr] &6{Sets the taggerspawn}");
		SMsg.sendMsg(p, "&e/fr pspawn [MapNr] &6{Sets the playerspawn}");
		SMsg.sendMsg(p, "&e/fr setlobby &6{Sets the lobbyspawn}");
	}

}
