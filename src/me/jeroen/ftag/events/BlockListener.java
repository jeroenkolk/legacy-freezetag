package me.jeroen.ftag.events;

import me.jeroen.ftag.Core;
import me.jeroen.ftag.utilities.SMsg;

import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Painting;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class BlockListener implements Listener{
	
	public Core plugin;
	public BlockListener(Core ins){
		plugin = ins;
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE){
			SMsg.sendMsg(e.getPlayer(), "&7&lYou are not permitted to place/break any blocks!");
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE){
			SMsg.sendMsg(e.getPlayer(), "&7&lYou are not permitted to place/break any blocks!");
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onItemBreak(HangingBreakByEntityEvent e){
		if (e.getRemover() instanceof Player){
			Player p = (Player) e.getRemover();
			if(p.getGameMode() != GameMode.CREATIVE){
				SMsg.sendMsg(p, "&7&lYou are not permitted to place/break any blocks!");
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onItemInteract(PlayerInteractEntityEvent e){
		Player p = e.getPlayer();
		Entity en = e.getRightClicked();
		
		if ((en instanceof ItemFrame || en instanceof Painting) && p.getGameMode() != GameMode.CREATIVE){
			SMsg.sendMsg(p, "&7&lYou are not permitted to place/break any blocks!");
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerDrop(PlayerDropItemEvent e){
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE){
			e.setCancelled(true);
		}
	}

}
