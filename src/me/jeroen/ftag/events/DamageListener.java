package me.jeroen.ftag.events;

import java.util.logging.Level;

import me.jeroen.coreplugin.api.API;
import me.jeroen.ftag.Core;
import me.jeroen.ftag.enums.GameState;
import me.jeroen.ftag.utilities.SMsg;
import me.jeroen.ftag.utilities.TeleportP;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.potion.PotionEffect;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;
import org.kitteh.tag.TagAPI;

public class DamageListener implements Listener {
	public Core plugin;

	public DamageListener(Core ins) {
		Core.log("Ender", Level.INFO);
		plugin = ins;
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e){
		if(e.getCause() == DamageCause.FALL || e.getCause() == DamageCause.SUFFOCATION || e.getCause() == DamageCause.DROWNING || e.getCause() == DamageCause.CONTACT){
			e.setDamage((double) 0);
			e.setCancelled(true);
		}
		
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onCommandPreProcess(PlayerCommandPreprocessEvent e){
		if (e.getMessage().startsWith("/pl") || e.getMessage().startsWith("pl") || e.getMessage().startsWith("/kil") || e.getMessage().startsWith("kil")){
			SMsg.sendMsg(e.getPlayer(), "&4You do not have the permission :)");
			e.setMessage(null);
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onListPing(ServerListPingEvent e) {
		if (Core.GAMESTATE == GameState.INGAME) {
			e.setMotd(ChatColor.GOLD + "In game!");
		} else if (Core.GAMESTATE == GameState.RESETTING) {
			e.setMotd(ChatColor.DARK_PURPLE + "Resetting");
		} else {
			e.setMotd(ChatColor.RED +Core.MapName);
		}

	}

	@EventHandler
	public void onEntityDamag(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			final Player dmger = (Player) e.getDamager();
			Player plyer = (Player) e.getEntity();

			if (Core.Freezers.contains(dmger.getName()) && !Core.Freezers.contains(plyer.getName())) {
				if (!Core.FrozenPlayers.contains(plyer.getName())) {
					if (Core.GAMESTATE == GameState.INGAME){
						int y = 0;

						if (plyer.getLocation().subtract(0, 1, 0).getBlock()
								.getType() == Material.AIR) {
							y = plyer.getLocation().getBlockY() - 1;
						} else {
							y = plyer.getLocation().getBlockY();
						}

						Core.FrozenLoc.put(plyer.getName(), new Location(plyer.getWorld(),
								plyer.getLocation().getBlockX(), y, plyer
									.getLocation().getBlockZ()));
						Core.FrozenPlayers.add(plyer.getName());
						TagAPI.refreshPlayer(plyer);
						
						
						if (!Core.CanGetDisks.containsKey(dmger.getUniqueId())){
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Core.instance, new Runnable() {
								public void run() {
									if (Core.CanGetDisks.containsKey(dmger.getUniqueId())){
										Core.CanGetDisks.remove(dmger.getUniqueId());
									}
								}
							}, 30);
							API.giveDisks(dmger.getName(), plugin.getConfig()
								.getInt("Config.giveCoinsTFreeze"));
						}else{
							if(Core.KickDiskF.containsKey(dmger.getUniqueId())){
								int old = Core.KickDiskF.get(dmger.getUniqueId());
								if (old == 5){
									dmger.kickPlayer(ChatColor.RED+"You are not allowed to disk farm!");
								}
								Core.KickDiskF.remove(dmger.getUniqueId());
								Core.KickDiskF.put(dmger.getUniqueId(), (old+1));
							}else{
								Core.KickDiskF.put(dmger.getUniqueId(), 1);
							}
							SMsg.sendMsg(dmger, ChatColor.RED+"You are not allowed to disk farm!");
						}
						
						Core.CanGetDisks.put(dmger.getUniqueId(), System.currentTimeMillis());
					}
				}
			} else if (Core.FrozenPlayers.contains(plyer.getName())
					&& !Core.FrozenPlayers.contains(dmger.getName())) {
				if (Core.GAMESTATE == GameState.INGAME){
					Core.FrozenPlayers.remove(plyer.getName());
					Core.FrozenLoc.remove(plyer.getName());
					TagAPI.refreshPlayer(plyer);
					
					if (!Core.CanGetDisks.containsKey(dmger.getUniqueId())){
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Core.instance, new Runnable() {
							public void run() {
								if (Core.CanGetDisks.containsKey(dmger.getUniqueId())){
									Core.CanGetDisks.remove(dmger.getUniqueId());
								}
							}
						}, 50);
						API.giveDisks(dmger.getName(), plugin.getConfig()
								.getInt("Config.giveCoinsHUnFreeze"));
					}else{
						if(Core.KickDiskF.containsKey(dmger.getUniqueId())){
							int old = Core.KickDiskF.get(dmger.getUniqueId());
							if (old == 5){
								dmger.kickPlayer(ChatColor.RED+"You are not allowed to disk farm!");
							}
							Core.KickDiskF.remove(dmger.getUniqueId());
							Core.KickDiskF.put(dmger.getUniqueId(), (old+1));
						}else{
							Core.KickDiskF.put(dmger.getUniqueId(), 1);
						}
						SMsg.sendMsg(dmger, ChatColor.RED+"You are not allowed to disk farm!");
					}
					
					Core.CanGetDisks.put(dmger.getUniqueId(), System.currentTimeMillis());
				}
			} else {
				if (Core.GAMESTATE == GameState.INGAME){
					SMsg.sendMsg(dmger, "&4You can not unfreeze someone when you are frozen!");	
				}
			}
			e.setDamage((double) 0);
			e.setCancelled(true);

		}
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		if (Core.FrozenPlayers.contains(e.getPlayer().getName()) && Core.GAMESTATE == GameState.INGAME) {
			if(e.getFrom().getBlockX()!=e.getTo().getBlockX() ||e.getFrom().getBlockZ()!=e.getTo().getBlockZ() )
			//if(e.getFrom().getBlock().getLocation() != e.getTo().getBlock().getLocation())
			e.getPlayer().teleport(Core.FrozenLoc.get(e.getPlayer().getName()));
		}
		e.getPlayer().setFoodLevel(20);
	}

	@EventHandler
	public void onNameTag(AsyncPlayerReceiveNameTagEvent e) {

		if (Core.Freezers.contains(e.getNamedPlayer().getName())){
			e.setTag(ChatColor.RED + e.getNamedPlayer().getName());
		}else if (Core.FrozenPlayers.contains(e.getNamedPlayer().getName())) {
			e.setTag(ChatColor.AQUA + e.getNamedPlayer().getName());
		} else {
			e.setTag(ChatColor.GREEN + e.getNamedPlayer().getName());
		}
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent e) {
		if (Core.FrozenPlayers.contains(e.getPlayer().getName())) {
			Core.FrozenPlayers.remove(e.getPlayer().getName());
		}
		if(Core.Freezers.contains(e.getPlayer().getName())){
			Core.Freezers.remove(e.getPlayer().getName());
		}
		e.getPlayer().getInventory().clear();
		e.setLeaveMessage(null);
	}

	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {
		if (Core.FrozenPlayers.contains(e.getPlayer().getName())) {
			Core.FrozenPlayers.remove(e.getPlayer().getName());
		}
		if(Core.Freezers.contains(e.getPlayer().getName())){
			Core.Freezers.remove(e.getPlayer().getName());
		}
		
		e.getPlayer().getInventory().clear();
		e.setQuitMessage(null);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		if (Core.GAMESTATE != GameState.PREGAME) {
			e.getPlayer().kickPlayer(ChatColor.RED + "Game Already started!");
			e.setJoinMessage(null);
			return;
		}

		e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', "&b"+e.getPlayer().getName()+" &3Is now playing FreezeTag"));

		if(Core.LobbySpawn!=null){
			e.getPlayer().teleport(Core.LobbySpawn);	
		}
		
		e.getPlayer().getInventory().clear();
		e.getPlayer().getInventory().setHelmet(null);
		e.getPlayer().getInventory().setChestplate(null);
		e.getPlayer().getInventory().setLeggings(null);
		e.getPlayer().getInventory().setBoots(null);
		
		for (PotionEffect effect : e.getPlayer().getActivePotionEffects())
			e.getPlayer().removePotionEffect(effect.getType());
		
		e.getPlayer().updateInventory();
	}

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent e) {
		if (Core.GAMESTATE != GameState.PREGAME) {
			e.getPlayer().kickPlayer(ChatColor.RED + "Game Already started!");
			e.setKickMessage(ChatColor.RED + "Game Already started!");
			e.setResult(Result.KICK_OTHER);
		}
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent e){
		if(Core.GAMESTATE == GameState.PREGAME){
			e.setRespawnLocation(Core.LobbySpawn);	
		}else{
			if(!Core.FrozenPlayers.contains(e.getPlayer().getName())){
				e.setRespawnLocation(TeleportP.tpPlayer(e.getPlayer()));	
			}else{
				e.setRespawnLocation(Core.FrozenLoc.get(e.getPlayer().getName()));
			}
		}
	}

}
