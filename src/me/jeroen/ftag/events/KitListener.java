package me.jeroen.ftag.events;

import java.util.Arrays;
import java.util.List;

import me.jeroen.coreplugin.api.API;
import me.jeroen.ftag.Core;
import me.jeroen.ftag.enums.GameState;
import me.jeroen.ftag.timers.KitTimer;
import me.jeroen.ftag.utilities.Kits;
import me.jeroen.ftag.utilities.MoreKit;
import me.jeroen.ftag.utilities.SMsg;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

public class KitListener implements Listener{
	
	private static List<String> liss = Arrays
			.asList(new String[] {ChatColor.DARK_PURPLE+"Left click to select a kit!"});
	
	public static ItemStack feather = setNameAn(new ItemStack(Material.FEATHER),
			ChatColor.BLUE + "" + ChatColor.BOLD + "Choose a kit", liss);

	public Core plugin;
	public KitListener(Core ins){
		plugin = ins;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		
		if(Core.GAMESTATE == GameState.PREGAME && e.getPlayer().getItemInHand().isSimilar(feather)){
			MoreKit.printKitChat(e.getPlayer());
			
		}else if(Core.GAMESTATE == GameState.INGAME && !KitTimer.KitTimeout.containsKey(e.getPlayer().getUniqueId())){
			if((e.getPlayer().getItemInHand().isSimilar(Kits.blazeRod) || e.getPlayer().getItemInHand().isSimilar(Kits.quartnet) || e.getPlayer().getItemInHand().isSimilar(Kits.starneth)) && !KitTimer.KitTimeout.containsKey(e.getPlayer().getName())){
				if(e.getPlayer().getItemInHand().isSimilar(Kits.blazeRod)){
					for (PotionEffect effect : e.getPlayer().getActivePotionEffects())
						e.getPlayer().removePotionEffect(effect.getType());
					KitTimer.KitTimeout.put(e.getPlayer().getUniqueId(), 60*10);
					SMsg.sendMsg(e.getPlayer(), "&eYou have now speed 4 four 10 seconds");
				}else if(e.getPlayer().getItemInHand().isSimilar(Kits.starneth)){
					API.giveDisks(e.getPlayer().getName(), Core.FrozenPlayers.size());
					Core.FrozenPlayers.clear();
					Core.FrozenLoc.clear();
					KitTimer.KitTimeout.put(e.getPlayer().getUniqueId(), 120*10);
					SMsg.sendMsg(e.getPlayer(), "&eYou have untawd all the players!");
				}else if(e.getPlayer().getItemInHand().isSimilar(Kits.quartnet)){
					for (Player p:Bukkit.getOnlinePlayers()){
						if(!p.getName().equals(e.getPlayer().getName())){
							p.hidePlayer(e.getPlayer());
						}
					}
					SMsg.sendMsg(e.getPlayer(), "&2&lVaniched!");
					KitTimer.KitTimeout.put(e.getPlayer().getUniqueId(), 60*10);
					SMsg.sendMsg(e.getPlayer(), "&eYou are now invisible for 10 seconds!");
				}
				
			}
		}else if((e.getPlayer().getItemInHand().isSimilar(Kits.blazeRod) || e.getPlayer().getItemInHand().isSimilar(Kits.quartnet) || e.getPlayer().getItemInHand().isSimilar(Kits.starneth)) && KitTimer.KitTimeout.containsKey(e.getPlayer().getUniqueId())){
			SMsg.sendMsg(e.getPlayer(), "&c&lYou can use your kit again in: "+KitTimer.KitTimeout.get(e.getPlayer().getUniqueId())/10+" seconds!");
		}
	}
	
	@EventHandler
	public void onPlayerInfInteract(InventoryInteractEvent e){
		if(Core.GAMESTATE == GameState.INGAME){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayJoin(PlayerJoinEvent e){
		if(!e.getPlayer().getInventory().contains(feather)){
			e.getPlayer().getInventory().setItem(0,feather);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		if(e.getItemDrop().getItemStack().isSimilar(feather)){
			e.setCancelled(true);
			e.getPlayer().updateInventory();
		}
	}
	
	
	
	
	
	
	
	private static ItemStack setNameAn(ItemStack is, String name,
			List<String> lore) {
		ItemMeta im = is.getItemMeta();
		if (name != null) {
			im.setDisplayName(ChatColor.DARK_PURPLE + name);
		}
		if (lore != null) {
			im.setLore(lore);
		}
		is.setItemMeta(im);

		return is;
	}

}
